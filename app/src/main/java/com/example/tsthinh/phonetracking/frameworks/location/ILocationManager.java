package com.example.tsthinh.phonetracking.frameworks.location;

/**
 * Created by thinh on 11/13/2016.
 */

public interface ILocationManager {
    void start();
    void stop();
}
