package com.example.tsthinh.phonetracking.presenter.map;

/**
 * Created by tsthinh on 11/10/2016.
 */

public interface IMapPresenter {
    void onTrackDevice();
    void onShowDevice();
}
