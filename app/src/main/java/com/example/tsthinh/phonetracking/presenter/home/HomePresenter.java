package com.example.tsthinh.phonetracking.presenter.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

import com.example.tsthinh.phonetracking.frameworks.location.LocationService;
import com.example.tsthinh.phonetracking.frameworks.net.INetworkListener;
import com.example.tsthinh.phonetracking.frameworks.net.INetworkProxy;
import com.example.tsthinh.phonetracking.model.Device;
import com.example.tsthinh.phonetracking.model.DeviceTracker;
import com.example.tsthinh.phonetracking.model.IDeviceTracker;
import com.example.tsthinh.phonetracking.model.map.Message;
import com.example.tsthinh.phonetracking.view.home.IHomeView;
import com.example.tsthinh.phonetracking.view.map.MapActivity;
import com.example.tsthinh.phonetracking.view.selectdevice.SelectDeviceActivity;
import com.example.tsthinh.phonetracking.view.signin.SignInActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.api.model.StringList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by tsthinh on 11/9/2016.
 */

public class HomePresenter implements IHomePresenter {
    private static final String TAG = "HomePresenter";
    private final int REQUEST_CODE = 1;
    private Context context;
    private IHomeView view;
    private Device runningDevice;
    private ArrayList<Device> devices;
    private Device trackingDevice;
    private IDeviceTracker deviceTracker;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mUsername;
    private String mPhotoUrl;

    public HomePresenter(Context context, IHomeView view) {
        this.context = context;
        this.view = view;
        devices = new ArrayList<>();
        runningDevice = new Device("10000", "Thinh");
    }

    @Override
    public void onAuthentication() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            context.startActivity(new Intent(context, SignInActivity.class));
            ((Activity) context).finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }
    }

    public void addDevice(Device device) {
        Log.d(TAG, "addDevice function call");
        devices.add(device);
    }

    public void selectDevice() {
        Log.d(TAG, "selectDevice function call");
        Intent intent = new Intent(context, SelectDeviceActivity.class);
        intent.putExtra("devices", devices);
        ((Activity) context).startActivityForResult(intent, REQUEST_CODE);
    }

    public void startTracking() {
        Log.d(TAG, "startTracking function call");
        if (null != trackingDevice) {
            Intent intent = new Intent(context, MapActivity.class);
            intent.putExtra("device", trackingDevice);
            context.startActivity(intent);
        }
    }

    public void stopTracking() {
        Log.d(TAG, "stopTracking function call");
        if (null != deviceTracker)
            deviceTracker.stop();
    }

    public void victim() {
        Intent intent = new Intent(context, LocationService.class);
        intent.putExtra("device", runningDevice);
        context.startService(intent);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_CODE == requestCode) {
            if (Activity.RESULT_OK == resultCode) {
                int index = data.getIntExtra("device_index", -1);
                if (-1 != index) {
                    trackingDevice = devices.get(index);
                }
            }
        }
    }

    @Override
    public void onReceived(String message) {
        Log.d(TAG, "onReceived function call");
        Log.d(TAG, "message: " + message);
    }
}
