package com.example.tsthinh.phonetracking.frameworks.net;

import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.logging.LogRecord;

/**
 * Created by tsthinh on 11/10/2016.
 * Ref: https://thinkandroid.wordpress.com/2010/03/27/incorporating-socket-programming-into-your-applications/
 */

public class Server {
    final String TAG = "Server";
    private String SERVER_IP;
    private final int SERVER_PORT = 8080;
    private Handler handler;
    private ServerSocket serverSocket;
    private String serverStatus;

    public Server() {
        SERVER_IP = getLocalIpAddress();
        handler = new Handler();

    }

    public class ServerThread implements Runnable {

        public void run() {
            try {
                if (SERVER_IP != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "Listening on IP: " + SERVER_IP);
                        }
                    });
                    serverSocket = new ServerSocket(SERVER_PORT);
                    while (true) {
                        // LISTEN FOR INCOMING CLIENTS
                        Socket client = serverSocket.accept();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "Connected.");
                            }
                        });

                        try {
                            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                            String line = null;
                            while ((line = in.readLine()) != null) {
                                Log.d(TAG, line);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        // DO WHATEVER YOU WANT TO THE FRONT END
                                        // THIS IS WHERE YOU CAN BE CREATIVE
                                    }
                                });
                            }
                            break;
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(TAG, "Oops. Connection interrupted. Please reconnect your phones.");
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "Couldn't detect internet connection.");
                        }
                    });
                }
            } catch (Exception e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "Error");
                    }
                });
                e.printStackTrace();
            }
        }
    }

    public void start() {
        Log.d(TAG, "start function call");
        Thread serverThread = new Thread(new ServerThread());
        serverThread.start();
    }

    public void stop() {
        Log.d(TAG, "stop function call");
        try {
            // MAKE SURE YOU CLOSE THE SOCKET UPON EXITING
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e(TAG, ex.toString());
        }
        return null;
    }
}
