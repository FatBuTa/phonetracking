package com.example.tsthinh.phonetracking.frameworks.net;

import com.example.tsthinh.phonetracking.model.Device;

import java.io.Serializable;

/**
 * Created by tsthinh on 11/10/2016.
 */

public class NetworkProxy implements INetworkProxy {
    private Device device;
    INetworkListener listener;
    private Server server;
    public NetworkProxy(INetworkListener listener, Device device) {
        this.listener = listener;
        this.device = device;
    }
    @Override
    public void Start() {
        if(null == server)
            server = new Server();
        server.start();
    }

    @Override
    public void Stop() {
        server.stop();
    }

    public void Send(String message) {

    }

    public void Receive(String message) {
        listener.onReceived(message);
    }
}
