package com.example.tsthinh.phonetracking.view.selectdevice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.example.tsthinh.phonetracking.R;
import com.example.tsthinh.phonetracking.presenter.selectdevice.ISelectDevicePresenter;
import com.example.tsthinh.phonetracking.presenter.selectdevice.SelectDevicePresenter;

/**
 * Created by tsthinh on 11/9/2016.
 */

public class SelectDeviceActivity extends AppCompatActivity implements ISelectDeviceView {
    private ISelectDevicePresenter presenter;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_device);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        presenter = new SelectDevicePresenter(this, this);
        presenter.onShowDeviceList(recyclerView);
    }
}