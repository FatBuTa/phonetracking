package com.example.tsthinh.phonetracking.frameworks.location;

import android.location.Location;

/**
 * Created by thinh on 11/13/2016.
 */

public interface ILocationListener {
    void onLocationChanged(Location location);
}
