package com.example.tsthinh.phonetracking.presenter.selectdevice;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.tsthinh.phonetracking.model.Device;
import com.example.tsthinh.phonetracking.view.selectdevice.DeviceListAdapter;
import com.example.tsthinh.phonetracking.view.selectdevice.DeviceListItemDecoration;
import com.example.tsthinh.phonetracking.view.selectdevice.ISelectDeviceView;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/9/2016.
 */

public class SelectDevicePresenter implements ISelectDevicePresenter {
    private final String TAG = "SelectDevicePresenter";
    private Context context;
    private ISelectDeviceView view;
    private ArrayList<Device> devices;

    public SelectDevicePresenter(Context context, ISelectDeviceView view) {
        this.context = context;
        this.view = view;
        Intent intent = ((Activity) context).getIntent();
        devices = (ArrayList<Device>)intent.getSerializableExtra("devices");
    }

    public void onShowDeviceList(RecyclerView recyclerView) {
        Log.d(TAG, "onShowDeviceList function call");
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.addItemDecoration(new DeviceListItemDecoration());
        recyclerView.setLayoutManager(layoutManager);
        RecyclerView.Adapter adapter = new DeviceListAdapter(this, devices);
        recyclerView.setAdapter(adapter);
    }

    public void onItemClick(int position) {
        Log.d(TAG, "onItemClick function call");
        Intent intent = new Intent();
        intent.putExtra("device_index", position);
        Activity activity = (Activity) context;
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
    }
}
