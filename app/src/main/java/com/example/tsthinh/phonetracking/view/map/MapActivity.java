package com.example.tsthinh.phonetracking.view.map;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.tsthinh.phonetracking.R;
import com.example.tsthinh.phonetracking.presenter.map.IMapPresenter;
import com.example.tsthinh.phonetracking.presenter.map.MapPresenter;

/**
 * Created by tsthinh on 11/9/2016.
 */

public class MapActivity extends AppCompatActivity implements IMapView {
    private IMapPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        presenter = new MapPresenter(this, this);
        presenter.onTrackDevice();
    }
}
