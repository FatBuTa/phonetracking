package com.example.tsthinh.phonetracking.presenter.home;

import android.content.Intent;

import com.example.tsthinh.phonetracking.model.Device;


/**
 * Created by tsthinh on 11/10/2016.
 */

public interface IHomePresenter {
    public void addDevice(Device device);

    public void selectDevice();

    public void startTracking();

    public void stopTracking();

    public void victim();

    public void onActivityResult(int requestCode, int resultCode, Intent data);

    public void onReceived(String message);

    public void onAuthentication();
}
