package com.example.tsthinh.phonetracking.presenter.selectdevice;

import android.support.v7.widget.RecyclerView;

/**
 * Created by tsthinh on 11/10/2016.
 */

public interface ISelectDevicePresenter {
    public void onShowDeviceList(RecyclerView recyclerView);
    public void onItemClick(int position);
}
