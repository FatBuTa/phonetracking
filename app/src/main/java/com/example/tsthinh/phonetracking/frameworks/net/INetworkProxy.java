package com.example.tsthinh.phonetracking.frameworks.net;

/**
 * Created by tsthinh on 11/10/2016.
 */

public interface INetworkProxy {
    public void Start();
    public void Stop();
}
