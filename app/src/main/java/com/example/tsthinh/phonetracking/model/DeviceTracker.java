package com.example.tsthinh.phonetracking.model;

import android.util.Log;

import com.example.tsthinh.phonetracking.frameworks.net.INetworkListener;
import com.example.tsthinh.phonetracking.frameworks.net.INetworkProxy;
import com.example.tsthinh.phonetracking.frameworks.net.NetworkProxy;
import com.example.tsthinh.phonetracking.model.Device;
import com.example.tsthinh.phonetracking.presenter.home.IHomePresenter;

/**
 * Created by tsthinh on 11/10/2016.
 */

public class DeviceTracker implements IDeviceTracker, INetworkListener{
    final String TAG = "DeviceTracker";
    private IHomePresenter presenter;
    private Device device;
    private INetworkProxy networkProxy;

    public DeviceTracker(IHomePresenter presenter, Device device) {
        this.device = device;
        networkProxy = new NetworkProxy(this, device);
    }
    public void start() {
        Log.d(TAG, "Start function call");
        networkProxy.Start();
    }
    public void stop() {
        Log.d(TAG, "Stop function call");
        networkProxy.Stop();
    }

    @Override
    public void onReady() {

    }

    @Override
    public void onReceived(String message) {
        presenter.onReceived(message);
    }
}
