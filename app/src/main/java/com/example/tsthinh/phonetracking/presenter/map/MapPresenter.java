package com.example.tsthinh.phonetracking.presenter.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.tsthinh.phonetracking.R;
import com.example.tsthinh.phonetracking.model.Device;
import com.example.tsthinh.phonetracking.view.map.IMapView;
import com.example.tsthinh.phonetracking.view.map.MapActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by tsthinh on 11/9/2016.
 */

public class MapPresenter implements IMapPresenter, OnMapReadyCallback {
    private static final String TAG = "MapPresenter";
    public static final String MESSAGES_CHILD = "messages";
    private Context context;
    private IMapView view;
    private Device trackingDevice;
    GoogleMap googleMap;

    private DatabaseReference mFirebaseDatabaseReference;

    public MapPresenter(Context context, IMapView view) {
        this.context = context;
        this.view = view;
        Intent intent = ((Activity)context).getIntent();
        trackingDevice = (Device)intent.getSerializableExtra("device");
        SupportMapFragment fm = (SupportMapFragment)
                ((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.map);

        // Getting GoogleMap object from the fragment
//        googleMap = fm.getMap();
        fm.getMapAsync(this);


    }

    @Override
    public void onTrackDevice() {
        Log.d(TAG, "onTrackDevice call");
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "Data changed: " + dataSnapshot.toString());

                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    Device device = null;
                    //Todo(Thinh): update this logic
                    for (DataSnapshot mess: messageSnapshot.getChildren()) {
                        device = (Device) mess.getValue(Device.class);
                    }
                    update(device);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void update(Device device) {
        Log.d(TAG, "update: device name: " + device.getId() + ", position: (" +
                device.getPosition_x() + ", " + device.getPosition_y() + ")");
        LatLng position = new LatLng(device.getPosition_x(),device.getPosition_y());
        if(googleMap != null) {
            Log.d(TAG, "update: googlemap is not null");
            googleMap.addMarker(new MarkerOptions().position(position).title("Start"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        }

    }
    @Override
    public void onShowDevice() {
        Log.d(TAG, "device name: " + trackingDevice.getId() + ", position: (" +
                trackingDevice.getPosition_x() + ", " + trackingDevice.getPosition_y() + ")");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
// Enabling MyLocation Layer of Google Map
        Log.d(TAG, "onMapReady function call");
        this.googleMap = googleMap;
//        googleMap.setMyLocationEnabled(true);
//        GoogleMap mMap = googleMap;
//
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
