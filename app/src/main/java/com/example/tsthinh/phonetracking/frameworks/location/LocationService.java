package com.example.tsthinh.phonetracking.frameworks.location;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.example.tsthinh.phonetracking.model.Device;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by thinh on 11/14/2016.
 */

public class LocationService extends IntentService implements ILocationListener{
    private static final String TAG = "LocationService";
    public static final String MESSAGES_CHILD = "messages";
    private DatabaseReference mFirebaseDatabaseReference;
    private Device runningDevice;
    ILocationManager locationManager;
    public LocationService() {
        super("location");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand function call");
        runningDevice = (Device)intent.getSerializableExtra("device");
        locationManager = new LocationManagerImpl(this, this);
        locationManager.start();
        return START_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.d(TAG, "onStart function call");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged function call");
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        if (null == runningDevice) {
            Log.e(TAG, "Error: running device is null");
            return;
        }
        runningDevice.setPosition_x(location.getLatitude());
        runningDevice.setPosition_y(location.getLongitude());
        mFirebaseDatabaseReference.child(MESSAGES_CHILD)
                .push().setValue(runningDevice);
    }
}