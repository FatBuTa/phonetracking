package com.example.tsthinh.phonetracking.model;

/**
 * Created by tsthinh on 11/10/2016.
 */

public interface IDeviceTracker {
    public void start();
    public void stop();
}
