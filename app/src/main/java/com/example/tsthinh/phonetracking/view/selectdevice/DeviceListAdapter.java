package com.example.tsthinh.phonetracking.view.selectdevice;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tsthinh.phonetracking.R;
import com.example.tsthinh.phonetracking.model.Device;
import com.example.tsthinh.phonetracking.presenter.selectdevice.SelectDevicePresenter;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/10/2016.
 */

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.ViewHolder> {
    private ArrayList<Device> mDataSet;
    SelectDevicePresenter presenter;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvId;
        public TextView tvName;

        public ViewHolder(View v) {
            super(v);
            tvId = (TextView) v.findViewById(R.id.tv_device_id);
            tvName = (TextView) v.findViewById(R.id.tv_device_name);
        }
    }

    public DeviceListAdapter(SelectDevicePresenter presenter, ArrayList<Device> myDataSet) {
        this.presenter = presenter;
        if (myDataSet != null)
            mDataSet = myDataSet;
        else
            mDataSet = new ArrayList<Device>();
    }

    public void add(int position, Device device) {
        mDataSet.add(position, device);
        notifyItemInserted(position);
    }

    @Override
    public DeviceListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_item, parent, false);
        DeviceListAdapter.ViewHolder viewHolder = new DeviceListAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DeviceListAdapter.ViewHolder holder, final int position) {
        final Device device = mDataSet.get(position);
        holder.tvId.setText(device.getId());
        holder.tvName.setText(device.getName());
        holder.tvId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
