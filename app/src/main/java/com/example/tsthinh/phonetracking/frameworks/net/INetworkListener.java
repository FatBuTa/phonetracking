package com.example.tsthinh.phonetracking.frameworks.net;

/**
 * Created by tsthinh on 11/10/2016.
 */

public interface INetworkListener {
    public void onReady();
    public void onReceived(String message);
}
