package com.example.tsthinh.phonetracking.view.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.tsthinh.phonetracking.R;
import com.example.tsthinh.phonetracking.model.Device;
import com.example.tsthinh.phonetracking.presenter.home.HomePresenter;
import com.example.tsthinh.phonetracking.presenter.home.IHomePresenter;

public class HomeActivity extends AppCompatActivity implements IHomeView {

    private final String TAG = "HomeActivity";

    private Button btnAddDevice;
    private Button btnSelectDevice;
    private Button btnStartTracking;
    private Button btnVictim;

    private IHomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btnAddDevice = (Button) findViewById(R.id.buttonAddDevice);
        btnSelectDevice = (Button) findViewById(R.id.buttonSelectDevice);
        btnStartTracking = (Button) findViewById(R.id.buttonStartTracking);
        btnVictim = (Button) findViewById(R.id.buttonVictim);
        presenter = new HomePresenter(this, this);
        btnAddDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "button AddDevice click");
                for(int i = 0; i < 20; i ++) {
                    Device device = new Device("" + i, "thinh " + i);
                    presenter.addDevice(device);
                }
            }
        });

        btnSelectDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "button Select Device click");
                presenter.selectDevice();
            }
        });

        btnStartTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "button Start Tracking click");
                presenter.startTracking();
            }
        });

        btnVictim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.victim();
            }
        });

        presenter.onAuthentication();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }
}
